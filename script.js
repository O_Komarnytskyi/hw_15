let a = ' '

let person = {
    name: 'John',
    age: 30,
    gender: 'male'
}
console.log(person)

let newPerson = {...person}
newPerson.age = 35
console.log(newPerson)

console.log(a)
console.log(a)
console.log(a)

let car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
}
console.log(car)

function printCarInfo(car) {
    console.log(`Make: ${car.make}, Model: ${car.model}, Year: ${car.year}`);
}

if (car.year < 2001) console.log('Машина занадто стара.')
else

    printCarInfo(car)

console.log(a)
console.log(a)
console.log(a)

let str = 'JavaScript is a high-level programming language';

function countWords(str) {
    return str.split(' ').length
}

console.log(countWords(str))


console.log(a)
console.log(a)
console.log(a)

let str2 = 'JavaScript is awesome!'
function reverseString(str2) {
    return str2.split('').reverse().join('')

}
console.log(reverseString(str2))

console.log(a)
console.log(a)
console.log(a)

let str3 = 'JavaScript is awesome!'
function reverseWordsInString(str3) {
    return str3.split(' ').reverse().join(' ').split('').reverse().join('')

}
console.log(reverseWordsInString(str3))